import tornado.ioloop
import tornado.web
import tornado.options
from tornado.options import define
import os
from tornado import template
import os
import cStringIO
from z3c.rml import rml2pdf
import tempfile

from uuid import uuid4

define("port", default=5000, help="run on the given port", type=int)
define("debug", default=False, type=bool)

PROJECT_DIR = os.path.dirname(__file__)


class HelloWorldHandler(tornado.web.RequestHandler):
    def get(self, name):
        loader = template.Loader(os.path.join(PROJECT_DIR, "templates", "reports"))
        rml_str = loader.load("hello_name.rml").generate(name=name)
        rendered_rml_file = tempfile.NamedTemporaryFile(delete=False)
        rendered_rml_file.write(rml_str)
        out_file = tempfile.NamedTemporaryFile()
        rendered_rml_file.close()
        rml2pdf.go(rendered_rml_file.name, outputFileName=out_file.name)
        buffer = out_file.read()
        out_file.close()
        os.unlink(rendered_rml_file.name)
        self.set_header('Content-Type', 'application/pdf')
        self.set_header('Content-Disposition', 'attachment; filename="hello_%s.pdf"' % name)
        self.write(buffer)

class POHandler(tornado.web.RequestHandler):
    def get(self):
        loader = template.Loader(os.path.join(PROJECT_DIR, "templates", "reports"))

        # Dynamic Data
        logo = os.path.join(PROJECT_DIR, "static", "img", "builk_logo.png")
        font_path = os.path.join(PROJECT_DIR, "static", "fonts", "THSarabunNew.ttf")
        n = self.get_argument('n', 4)
        n = int(n)
        items = [['item', 1, 'o', '100,000', '-', '100,000']]*n

        rml_str = loader.load("po.rml").generate(logo=logo, items=items, font_path=font_path)
        rendered_rml_file = tempfile.NamedTemporaryFile(delete=False)
        rendered_rml_file.write(rml_str)
        out_file = tempfile.NamedTemporaryFile()
        rendered_rml_file.close()
        rml2pdf.go(rendered_rml_file.name, outputFileName=out_file.name)
        buff = out_file.read()
        out_file.close()
        os.unlink(rendered_rml_file.name)
        self.set_header('Content-Type', 'application/pdf')
        self.set_header('Content-Disposition', 'filename="po.pdf"')
        self.write(buff)

settings = {
    'cookie_secret': 'aa499a2ea454488f9e262c11f6b71372',
    'template_path': os.path.join(PROJECT_DIR, "templates"),
    'static_path': os.path.join(PROJECT_DIR, "static"),
}

if __name__ == "__main__":
    tornado.options.parse_command_line()
    application = tornado.web.Application([
                                              (r"/reports/hello_(\w+)", HelloWorldHandler),
                                              (r"/reports/po", POHandler)
                                          ], debug=tornado.options.options.debug, **settings)
    application.listen(tornado.options.options.port)
    tornado.ioloop.IOLoop.instance().start()