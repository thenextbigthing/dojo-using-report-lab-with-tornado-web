from tornado import template
import os
import cStringIO
from z3c.rml import rml2pdf
import tempfile

from uuid import uuid4

PROJECT_DIR = os.path.dirname(__file__)
loader = template.Loader(os.path.join(PROJECT_DIR, "templates", "reports"))
rml_str = loader.load("hello_world.rml").generate()
rendered_rml_file = tempfile.NamedTemporaryFile(delete=False)
rendered_rml_file.write(rml_str)
out_file = tempfile.NamedTemporaryFile()

rendered_rml_file.close()


rml2pdf.go(rendered_rml_file.name, outputFileName=out_file.name)

print out_file.read()

out_file.close()
print rendered_rml_file.name
os.unlink(rendered_rml_file.name)

